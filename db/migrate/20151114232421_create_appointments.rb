class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      # t.references :user, :advisor, index: true, foreign_key: true
      t.references :note, index: true, foreign_key: true

      t.datetime :start_time, null: false
      t.datetime :end_time

      t.timestamps null: false
    end
  end
end
