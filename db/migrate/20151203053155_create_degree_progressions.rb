class CreateDegreeProgressions < ActiveRecord::Migration
  def change
    create_table :degree_progressions do |t|
      t.references :user, index: true, foreign_key: true
      t.references :course, index: true, foreign_key: true
      t.boolean :is_complete

      t.timestamps null: false
    end
  end
end
