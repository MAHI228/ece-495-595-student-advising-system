class AddMajorToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :major, index: true, foreign_key: true
    add_reference :users, :minor, index: true, foreign_key: true
  end
end
