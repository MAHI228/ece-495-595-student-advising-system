class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :edit_student, :update, :destroy]
  authorize_resource


  
  def index
    @users = User.all
  end


  def show
  end

  def new
    @user = User.new
  end

  def edit
  end

  def edit_student
    
  end

  
  def create
    @user = User.new(user_params)

    if @user.save
      # since at this point we dont really care about the validation of creds
      # we can do this or soemthing else
      redirect_to(@user, :notice => 'User was successfully created.')
      redirect_to(new_user_session_path)
    else
      render :action => "new" 
    end
    
  end

  def update
    # @user = current_user

    if @user.update_attributes(user_params)
      redirect_to(@user, :notice => 'User was successfully updated.') 
    else
      render :action => "edit"
    end
  end

  # def update
    
  #   if @user.update_attributes(user_params)
  #     redirect_to(@user, :notice => 'User was successfully updated.') 
  #   else
  #     render :action => "edit"
  #   end
  # end

 
  def destroy
    # @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => "pages") }
    end
  end

  private
  def set_user
    @user = User.find(params[:id])
    # if @user.roles.size == 0
    #   @roles = Role.all
    # else
    #   @roles = @user.roles
    # end
    # @roles = Role.all

  end
  
  def user_params
    # note the nested array param!!!!!!
    # TODO - this is a security concern if the user is a student
    # we should not permit department for students.
    params.require(:user).permit(:username,
                                 :email,
                                 :first_name,
                                 :last_name,
                                 :department,
                                 :major_id,
                                 :minor_id,
                                 :department_id,
                                 :role_ids => [])
  end

end
